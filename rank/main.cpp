#include <iostream>
#include <vector>
#include <algorithm>


int main()
{
    int k, n;
    std::cin >> k >> n;
    std::vector<std::pair<int64_t, int64_t>> input(n);
    for (int i = 0; i < n; i++)
    {
        std::cin >> input[i].first >> input[i].second;
        input[i].second--;
    }
    std::sort(input.begin(), input.end());
    std::vector<std::vector<int64_t>> matrix(k);
    std::vector<int> indexes(k,0);
    for (int i = 0; i < n; i++)
        matrix[input[i].second].push_back(input[i].first);
    long long sum = 0;
    long long ans = 0;
    for (int i = 0; i < n - 1; i++)
    {
        sum += (input[i+1].first - input[i].first) * (n - i - 1) * (i + 1);
        int sz1 = xs[x[i].second].size();
        int ind1 = ind[x[i].second]++;
        if (sz1 != ind1 + 1)
            ans += (xs[x[i].second][ind1 + 1] - xs[x[i].second][ind1]) * (sz1 - ind1 - 1) * (ind1 + 1);
    }
    std::cout << 2 * ans << std::endl << 2 * (sum - ans) << std::endl;
}