import math

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier

from sklearn.model_selection import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.svm import SVC


train = pd.read_csv('train.csv', sep=',')
test = pd.read_csv('test.csv', sep=',')
Xt = train.iloc[:,0:28].copy()
Yt = train['class'].copy()
categorical = Xt.columns.values[-7:]
numerical = Xt.columns.values[0:21]



transform_dict = {}
for col in categorical:
    cats = pd.Categorical(train[col]).categories
    d = {}
    for i, cat in enumerate(cats):
        d[cat] = i
    transform_dict[col] = d

XtLE = Xt.replace(transform_dict)

Ids = test['id']
X = test.iloc[:,:]
X = X.replace(transform_dict)
processed_X = np.concatenate([
    StandardScaler().fit_transform(X[numerical]),
    OneHotEncoder(sparse=False).fit_transform(X[categorical])
], axis=1)

numerical_scaled = StandardScaler().fit_transform(Xt[numerical])
categorical_ohe = OneHotEncoder(sparse=False).fit_transform(XtLE[categorical])

processed_Xt = np.concatenate([numerical_scaled, categorical_ohe], axis=1)

def bestSVC(): # {'C': 1000, 'gamma': 0.001, 'kernel': 'rbf'}
    #tuned_parameters = [{'kernel': ['rbf', 'sigmoid', 'linear'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]}]
    #clf = GridSearchCV(SVC(), tuned_parameters, cv=5, scoring='accuracy', n_jobs=-1)
    #clf.fit(processed_Xt, Yt)
    #print(clf.best_params_)
    #y_true, y_pred = Yt, clf.predict(processed_Xt)
    #print('Accuracy: %s' % accuracy_score(y_true, y_pred))

    clf = SVC(C=1000, gamma=0.001, kernel='rbf')
    clf.fit(processed_Xt, Yt)

    return clf

def bestKNN(): # 21, acc = 0.58
    #tuned_parameters = [{'n_neighbors': range(2, 30)}]
    #clf = GridSearchCV(KNeighborsClassifier(), tuned_parameters, cv=5, scoring='accuracy', n_jobs=-1)
    #clf.fit(processed_Xt, Yt)
    #print(clf.best_params_)
    #means = clf.cv_results_['mean_test_score']
    #stds = clf.cv_results_['std_test_score']
    #for mean, std, params in zip(means, stds, clf.cv_results_['params']):
    #    print("%0.3f (+/-%0.03f) for %r"
    #          % (mean, std * 2, params))
    #print()

    clf = KNeighborsClassifier(n_neighbors=21)
    clf.fit(processed_Xt, Yt)

    return clf

def bestRandomForest():
    #tuned_parameters = [{'n_estimators': [10, 100, 200]}]
    #clf = GridSearchCV(RandomForestClassifier(), tuned_parameters, cv=5, scoring='accuracy', n_jobs=-1)
    #clf.fit(processed_Xt, Yt)
    #print(clf.best_params_)
    #means = clf.cv_results_['mean_test_score']
    #stds = clf.cv_results_['std_test_score']
    #for mean, std, params in zip(means, stds, clf.cv_results_['params']):
    #    print("%0.3f (+/-%0.03f) for %r"
    #          % (mean, std * 2, params))
    #print()

    clf = RandomForestClassifier(n_estimators=200)
    clf.fit(processed_Xt, Yt)

    return clf

def bestGD():
    return 1

svc = bestSVC()
knn = bestKNN()
forest = bestRandomForest()
#gb = bestGD()

Y = forest.predict(processed_X)
result = pd.DataFrame(data=[Ids, Y])
result.transpose().to_csv(path_or_buf='answer.csv', index=False)
print(1)