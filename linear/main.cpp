#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <numeric>
#include <iterator>
#include <iostream>

const double PI = 3.14159265358979323846;
const double EPS = 1e-8;
const double myInfinity = 1e10;

template <class T>
std::vector <std::vector<T>> Multiply(std::vector <std::vector<T>> &a, std::vector <std::vector<T>> &b)
{
    const int n = a.size();     // a rows
    const int m = a[0].size();  // a cols
    const int p = b[0].size();  // b cols

    std::vector <std::vector<T>> c(n, std::vector<T>(p, 0));
    for (auto j = 0; j < p; ++j)
    {
        for (auto k = 0; k < m; ++k)
        {
            for (auto i = 0; i < n; ++i)
            {
                c[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    return c;
}

class linearSolution {
    int n = 0,l = 0;
    std::vector<double> mean;
    std::vector<double> dev;
    std::vector<std::vector<double>> xs;
    std::vector<std::vector<double>> xsT;
    std::vector<double> ys;

    std::vector<double> coeffs;

public:
    explicit linearSolution(std::istream &input) {
        input >> n >> l;
        xs.resize(l, std::vector<double>(n + 1));
        xsT.resize(n+1, std::vector<double>(l));

        ys.resize(l);

        for (int i = 0; i < l; i++) {
            for (int j = 0; j < n; j++) {
                input >> xs[i][j];
            }

            input >> ys[i];
            xs[i][n] = xsT[n][i] = 1;
        }
    }

    void normalize() {
        mean.resize(n);
        dev.resize(n);

        for(int i = 0; i<n;i++) {
            for(int j = 0; j<l;j++) mean[i] += xs[j][i];

            mean[i] /= l;
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < l; j++) {
                double x = xs[j][i] - mean[i];
                dev[i] += x * x;
            }
            dev[i] = dev[i] < EPS ? myInfinity : sqrt(dev[i] / (l - 1));
        }

        for (int i = 0; i < l; i++) {
            for (int j = 0; j < n; j++) {
                xs[i][j] = (xs[i][j] - mean[j]) / dev[j];
            }
        }
    }

    auto mul(std::vector<std::vector<double>> a, std::vector<std::vector<double>> b) {
        return Multiply(a,b);
    }

    void computeCoeffcs() {
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < n; j++) {
                xsT[j][i] = xs[i][j];
            }
        }

        auto matrix = Multiply(xsT, xs);
        for(int i = 0; i <= n; i++) matrix[i][i] += 0.1;

        matrix = gauss(matrix);
        matrix = mul(matrix, xsT);

        coeffs.resize(n+1);
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j < l; j++) {
                coeffs[i] += matrix[i][j] * ys[j];
            }
        }
        for (int i = 0; i < n; i++) {
            if (dev[i] == myInfinity) {
                coeffs[i] = 0;
            } else {
                coeffs[i] /= dev[i];
                coeffs[n] -= coeffs[i] * mean[i];
            }
        }
    }

    std::vector<std::vector<double>> gauss(std::vector<std::vector<double>> matrix) {
        auto n = matrix.size();
        std::vector<std::vector<double>> res(n, std::vector<double>(n));
        for(int i = 0; i < n; i++) res[i][i] = 1;
        for(int i = 0; i < n; i++) {
            auto maxI = i;
            for(int j = i; j< n; j++) {
                if (matrix[j][i] > matrix[maxI][i]) {
                    maxI = j;
                }
            }

            std::swap(matrix[maxI], matrix[i]);
            std::swap(res[maxI], res[i]);

            auto delim = matrix[i][i];

            for (int j = 0; j < n; j++) {
                matrix[i][j] /= delim;
                res[i][j] /= delim;
            }
            for (int j = i + 1; j < n; j++) {
                delim = -matrix[j][i];
                for (int k = 0; k < n; k++) {
                    matrix[j][k] += delim * matrix[i][k];
                    res[j][k] += delim * res[i][k];
                }
            }
        }

        for (int i = n - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                auto delim = -matrix[j][i];
                for (int k = 0; k < n; k++) {
                    matrix[j][k] += delim * matrix[i][k];
                    res[j][k] += delim * res[i][k];
                }
            }
        }
        return res;
    }

    void output(std::ostream &out) {
        std::copy(coeffs.begin(), coeffs.end(), std::ostream_iterator<double>(out, "\n"));
    }
};

int main() {
    auto linear = linearSolution(std::cin);
    linear.normalize();
    linear.computeCoeffcs();
    linear.output(std::cout);
    return 0;
}