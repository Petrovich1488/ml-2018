#include <iostream>
#include <vector>
#include <cmath>
#include <random>
#include <ctime>

class DNN {
public:
    class A {
    public:
        std::vector<double> f;
        std::vector<double> y;

        A(const std::vector<double> & f, const std::vector<double> & y) : f(f), y(y) {        }
    };

    class Neuron {
    public:
        int i;
        std::vector<double> w;
        double fsum, dfsum, e;

        Neuron(int i):i(i) {
        }

        Neuron() {
            i = 0;
        }
    };


    int d,n,m,l;
    std::vector<int> dd;
    std::vector<std::vector<Neuron>> network;
    std::vector<double> means;
    std::vector<double> dev;
    std::vector<A> train;
    double lr = 0.05;

    DNN(std::istream &in) {
        in >> d;
        dd.resize(d);
        for(int i = 0; i<d;i++) in>>dd[i];
        in >> l;
        n = dd[0];
        m = dd[d - 1];
        for (int i = 0; i < l; i++) {
            std::vector<double> f(n);
            for (int j = 0; j < n; j++) {
                in>>f[j];
            }
            std::vector<double> y(m);
            for (int j = 0; j < m; j++) {
                in>>y[j];
            }
            train.push_back(A(f, y));
        }
    }

    void normalize() {
        int ll = train.size();
        int nn = train[0].f.size();
        means.resize(n);
        dev.resize(n);
        for (int i = 0; i < nn; i++) {
            for (auto &a : train) {
                means[i] += a.f[i];
            }
            means[i] /= l;
        }
        for (int i = 0; i < nn; i++) {
            for (auto &a : train) {
                double x = a.f[i] - means[i];
                dev[i] += x * x;
            }
            dev[i] = dev[i] < 1e-6 ? 1e9 : sqrt(dev[i] / (ll - 1));
        }

        for (int i = 0; i < n; i++) {
            for (auto &a : train) {
                a.f[i] = (a.f[i] - means[i]) / dev[i];
            }
        }
    }

    void fit() {
        network.resize(d);
        for (int i = 0; i < d; i++) {
            network[i] = std::vector<Neuron>(dd[i] + (i < d - 1 ? 1 : 0));
            for (int j = 0; j < dd[i]; j++) {
                Neuron ne(j);
                if (i > 0) {
                    ne.w = std::vector<double>(dd[i - 1] + 1);
                    for (int k = 0; k <= dd[i - 1]; k++) {
                        double x = sqrt(6.0 / (dd[i - 1] + dd[i]));
                        ne.w[k] = ((2 * (double)rand() / RAND_MAX) - 1) * x;
                    }
                }
                network[i][j] = ne;
            }
            if (i < d - 1) {
                Neuron ne(dd[i]);
                ne.fsum = 1;
                network[i][dd[i]] = ne;
            }
        }
        double q = 100;
        int step = 0;
        while (q > 1e-4) {
            A a = train[rand() % l];
            step++;
            double x = _fit(a, step);
            q = (1 - 1.0 / l) * q + 1.0 / l * x;
            if (step > 2e5) {
                break;
            }
        }
        for (int i = 0; i < dd[1]; i++) {
            for (int j = 0; j < n; j++) {
                network[1][i].w[n] -= means[j] / dev[j] * network[1][i].w[j];
                network[1][i].w[j] /= dev[j];
            }
        }
    }

    double _fit(A a, int step) {
        forward(a);
        backward(a);
        double x = 0;
        for (int j = 0; j < m; j++) {
            x += 0.5 * network[d - 1][j].e * network[d - 1][j].e;
        }
        return x;
    }

    void forward(A a) {
        for (int i = 0; i < dd[0]; i++) {
            network[0][i].fsum = a.f[i];
            network[0][i].e = 0;
        }
        for (int i = 1; i < d; i++) {
            for (int j = 0; j < dd[i]; j++) {
                double sum = 0;
                network[i][j].e = 0;
                for (Neuron &nee : network[i - 1]) {
                    sum += nee.fsum * network[i][j].w[nee.i];
                }
                network[i][j].fsum = tanhf(sum);
                network[i][j].dfsum = 1 - network[i][j].fsum * network[i][j].fsum;
//                std::cout << network[i][j].dfsum << " - " << network[i][j].fsum << std::endl;
            }
        }
    }

    void backward(A a) {
        for (Neuron &ne : network[d - 1]) {
            ne.e = ne.fsum - a.y[ne.i];
        }
        for (int i = d - 1; i > 0; i--) {
            for (int j = 0; j < dd[i]; j++) {
                for (Neuron &nee : network[i - 1]) {
                    int k = nee.i;
                    nee.e += network[i][j].e * network[i][j].dfsum * network[i][j].w[k];
                    double grad = network[i][j].e * network[i][j].dfsum * nee.fsum;
                    network[i][j].w[k] -= lr * grad;
                }
            }
        }
    }

    void output(std::ostream &out) {
        for (int i = 1; i < d; i++) {
            for (int j = 0; j < dd[i]; j++) {
                Neuron ne = network[i][j];
                for (double w : ne.w) {
                    out << w << " ";
                }
                out << std::endl;
            }
        }
    }
};

int main() {
    srand(time(NULL));
    DNN dnn(std::cin);
    dnn.normalize();
    dnn.fit();
    dnn.output(std::cout);
    return 0;
}