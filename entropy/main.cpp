#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <map>
#include <cmath>

int main()
{
    int k, m, n;
    std::cin >> k >> m >> n;
    std::map<std::pair<int, int>, int> map;
    std::vector<double> co(k);
    for (int i = 0; i < n; i++)
    {
        int a1,a2;
        std::cin >> a1 >> a2;
        auto tempPair = std::make_pair(a1 - 1, a2 - 1);
        if (map.find(tempPair) == map.end()) {
            map[tempPair] = 0;
        }

        map[tempPair]++;
        co[a1 - 1]++;
    }
    double ans = 0, dd= 0;
    int last = 0;
    for (auto &entry : map) {
        auto i = entry.first.first;
        if (last != i) {
            ans += co[last] * dd;
            dd = 0;
            last = i;
        }

        auto j = entry.first.second;
        auto temp = entry.second / co[i];
        dd += temp * log(temp);
    }

    ans = (ans + co[last] * dd) / (double) n;
    std::cout.precision(16);
    std::cout << std::fixed << -ans;
}