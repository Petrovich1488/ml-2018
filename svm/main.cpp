#define _USE_MATH_DEFINES

#include <iostream>
#include <vector>
#include <functional>
#include <cmath>
#include <numeric>
#include <iterator>
#include <algorithm>
#include <string>

class point {
public:
    int i;
    std::vector<double> features;
    double clazz;

    point(int i, std::vector<double> features, double y): i(i), features(features), clazz(y) {}
};

class sample {
public:
    std::vector<point> train;
    std::vector<point> test;

    sample(std::vector<point> train, std::vector<point> test): train(train), test(test) {}
    sample() {}
};

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}


namespace utils {
    class Kernel {
    public:
        double param;
        Kernel(double param) : param(param){}

        virtual double compute(const point &a, const point &b) = 0;

        virtual std::string toString(std::string in, int, const std::vector<double>&) = 0;
    };

    class LinearKernel : public Kernel {
    public:
        LinearKernel(double param) : Kernel(param) {}

        double compute(const point &a, const point &b) override {
            return std::inner_product(a.features.begin(), a.features.end(), b.features.begin(), 0.0);
        }
        std::string toString(std::string in, int, const std::vector<double>&) override {
            return in;
        }
    };

    class PolynomialKernel : public Kernel {
    public:
        PolynomialKernel(double param) : Kernel(param) {}

        double compute(const point &a, const point &b) override {
            double res = 1;
            double x = std::inner_product(a.features.begin(), a.features.end(), b.features.begin(), 0.0);
            for(int i = 0;i < param; i++) {
                x *= param;
            }

            return x;
        }

        std::string toString(std::string in, int, const std::vector<double>&) override {
            return "pow(sum(" + in + ",1)," + std::to_string(param) + ")";
        }
    };

    class RadialKernel : public Kernel {
    public:
        RadialKernel(double param) : Kernel(param) {}

        double compute(const point &a, const point &b) override {
            double res = 0;
            for(int i =0;i<a.features.size();i++) {
                res += pow(a.features[i] - b.features[i], 2);
            }
            res *= param;
            return pow(M_E, -res);
        }

        std::string toString(std::string in, int n,const std::vector<double> &dev)  override{
            return "pow(" + std::to_string(M_E) + ", prod(" + std::to_string(-param) + "," + writeNorm(n, dev) + "))";
        }

        std::string writeNorm(int n, const std::vector<double>& dev) {
            std::string s;
            s += "sum(";
            for (int i = 0; i < n; i++) {
                s += "pow(";
                s += "prod(";
                s += "sub(A" + std::to_string(i) + ",B" + std::to_string(i) + ")";
                s += "," + std::to_string(1 / dev[i]);
                s += "," + std::to_string(1 / dev[i]) + ")";
                s += ",2)";
                if (i != n - 1) {
                    s += ",";
                }
            }
            s += ")";
            return s;
        }

    };

    double calcF(const std::vector<double> &pred, const std::vector<double> &y) {
        double tp = 0;
        double fp = 0;
        double fn = 0;
        double counter = 0;
        for (int i = 0; i < y.size(); i++) {
            tp += (y[i] == 1 && pred[i] == 1);
            fn += (y[i] == 1 && pred[i] == -1);
            fp += (y[i] == -1 && pred[i] == 1);
            counter += y[i] == pred[i];
        }
        if (counter == y.size()) {
            return 1;
        }
        double pr = tp == 0 ? 0 : tp / (tp + fp);
        double re = tp == 0 ? 0 : tp / (tp + fn);
        return 2 / (1 / pr + 1 / re);
    }
}

std::vector<utils::Kernel *> kernels = {
        new utils::LinearKernel(1),
        new utils::PolynomialKernel(2),
        new utils::PolynomialKernel(5),
        new utils::RadialKernel(0.5),
        new utils::RadialKernel(2),
        new utils::RadialKernel(0.1),
        new utils::RadialKernel(10)
};

class svm {
    std::vector<double> means;
    std::vector<double> dev;
    std::vector<std::vector<double>> dist;
    std::vector<point> data;
    utils::Kernel *bestKer;
    utils::Kernel *curKer;
    double bestC = 0;
    double bestF = 0;
    int n, l;
    int kFold;
    int tFold;
    std::vector <double> cValues{1, 10, 0.1, 1000, 0.01};

public:
    svm(std::istream& in, int kFold, int tFold) : kFold(kFold), tFold(tFold), bestKer(kernels[0]) {
        in >> n >> l;
        means.resize(n);
        dev.resize(n);
        std::string temp;

        for(int i = 0;i < l;i++) {
            auto features = std::vector<double>(n);
            for(int j = 0; j < n; j++) in >> features[j];
            in >> temp;
            data.emplace_back(i, features, temp == "+" ? 1 : -1);
        }

        dist.resize(l, std::vector<double>(l));
    }

    svm(std::istream &in) : svm(in, 3, 3) {
    }

    void normalize() {
        for (int i = 0; i < n; i++) {
            for (auto &a : data) {
                means[i] += a.features[i];
            }
            means[i] /= l;
        }
        for (int i = 0; i < n; i++) {
            for (auto &a : data) {
                double x = a.features[i] - means[i];
                dev[i] += x * x;
            }
            dev[i] = dev[i] < 1e-6 ? 1e9 : sqrt(dev[i] / (l - 1));
        }

        for (int i = 0; i < n; i++) {
            for (auto &a : data) {
                a.features[i] = (a.features[i] - means[i]) / dev[i];
            }
        }
    }

private:
    std::vector<int> randomPerm (int l) {
        std::vector<int> res(l);
        std::iota(res.begin(), res.end(), 0);
        std::random_shuffle(res.begin(), res.end());

        return res;
    }
public:
    void fit() {
        for (auto kernelx: kernels) {
            curKer = kernelx;
            for (auto &c: cValues) {
                double f = 0;
                for (int iTFold = 0; iTFold < tFold; iTFold++) {
                    auto samples = CV(kFold);
                    for (auto &sample: samples) {
                        auto lambda = _fit(sample.train, c, 5);
                        double w0 = computeW0(sample.train, lambda, c);
                        std::vector<double> pred(sample.test.size());
                        std::vector<double> y(sample.test.size());
                        for (int i = 0; i < sample.test.size(); i++) {
                            auto x = predict(sample.test[i], sample.train, lambda) - w0;
                            pred[i] = sgn(x);
                            y[i] = sample.train[i].clazz;
                        }
                        f += utils::calcF(pred, y);
                    }
                }

                f /= tFold * kFold;
                if (f > bestF) {
                    bestF = f;
                    bestC = c;
                    bestKer = curKer;
                }
            }
        }
    }

    void output(std::ostream &out) {
        curKer = bestKer;
        out << bestKer->toString(scalarProduct(), n, dev) << std::endl;
        auto lambda = _fit(data, bestC, 100);
        auto w0 = computeW0(data ,lambda, bestC);
        std::copy(lambda.begin(), lambda.end(), std::ostream_iterator<double>(out, "\n"));
        out << w0 << " ";
    }

private:

    std::string scalarProduct() {
        std::string s;
        s += "sum(";
        for (int i = 0; i < n; i++) {
            s += "prod(";
            s += "sub(A" + std::to_string(i) + "," + std::to_string(means[i]) + ")";
            s += ",sub(B" + std::to_string(i) + "," + std::to_string(means[i]) + ")";
            s += "," + std::to_string(1 / pow(dev[i], 2)) + ")";
            if (i != n - 1) {
                s += ",";
            }
        }
        s += ")";
        return s;
    }

    std::vector<sample> CV(int k) {
        auto list = std::vector<point>(data.begin(), data.end());
        std::sort(list.begin(), list.end(), [](const point& a, const point &b) {
            return b.clazz > a.clazz;
        });
        int numNeg = std::count_if(data.begin(), data.end(), [](const point &a) {
            return a.clazz == -1;
        });
        int numPos = data.size() - numNeg;
        auto perm1 = randomPerm(numNeg);
        auto perm2 = randomPerm(numPos);
        std::vector<std::vector<point>> folds(k, std::vector<point>());
        int ii = 0;
        for (int i = 0; i < numNeg; i++) {
            folds[ii++].push_back(list[perm1[i]]);
            ii %= k;
        }
        for (int i = 0; i < numPos; i++) {
            folds[ii++].push_back(list[numNeg + perm2[i]]);
            ii %= k;
        }
        std::vector<sample> samples(k);
        for (int i = 0; i < k; i++) {
            std::vector<point> train;
            std::vector<point> test(folds[i]);
            for (int j = 0; j < k; j++) {
                if (j != i) {
                    for(auto ss: folds[j]) {
                        train.push_back(ss);
                    }
                }
            }
            samples[i] = sample(train, test);
        }
        return samples;
    }


    double predict(const point &a, const std::vector<point> &train, std::vector<double> lambda) {
        double res = 0;
        for (int i = 0; i < train.size(); i++) {
            if (lambda[i] > 1e-8) {
                res += lambda[i] * train[i].clazz * curKer->compute(a, train[i]);
            }
        }
        return res;
    }

    std::vector<double> _fit(const std::vector<point> &train, double c, int max) {
        std::vector<double> classes;
        auto ll = train.size();

        for(auto &t: train) {
            classes.push_back(t.clazz);
        }

        for (int i = 0; i < train.size(); i++) {
            for (int j = i; j < train.size(); j++) {
                dist[i][j] = dist[j][i] = curKer->compute(train[i], train[j]);
            }
        }

        std::vector<double> lambda(ll);
        double min = 10000000;

        for(int i=0;i<max;i++) {
            auto perm = randomPerm(ll);
            for (int j = 0; j < ll; j++) {
                for (int k = j + 1; k < ll; k++) {
                    int a1 = perm[j];
                    int a2 = perm[k];
                    tryMinimize(a1, a2, lambda, classes, c);
                }
            }

            auto langragianValue = lagrangian(train, lambda);
            if (langragianValue >= min - 1e-8) {
                break;
            }

            min = langragianValue;
        }

        return lambda;
    }

    void tryMinimize(int a1, int a2, std::vector<double> &lambda, const std::vector<double> &classes, double c) {
        l = lambda.size();
        double top;
        double bottom;
        if (classes[a1] == classes[a2]) {
            top = fmax(0, lambda[a1] + lambda[a2] - c);
            bottom = fmin(c, lambda[a1] + lambda[a2]);
        } else {
            top = fmax(0, lambda[a2] - lambda[a1]);
            bottom = fmin(c, c + lambda[a2] - lambda[a1]);
        }
        if (bottom - top < 1e-8) {
            return;
        }
        double sameClass = classes[a1] * classes[a2];
        double width = 2 * dist[a1][a2] - dist[a1][a1] - dist[a2][a2];
        if (fabs(width) < 1e-8) {
            return;
        }
        double shh = 0;
        for (int i = 0; i < l; i++) {
            shh += lambda[i] * classes[i] * (dist[a1][i] - dist[a2][i]);
        }
        shh *= classes[a2];
        shh += 1 - sameClass;
        double step = -shh / width;
        double change = lambda[a2] + step;
        change = fmax(change, top);
        change = fmin(change, bottom);
        lambda[a1] += sameClass * (lambda[a2] - change);
        lambda[a2] = change;
    }

    double computeW0(const std::vector<point> &train, const std::vector<double> &lambda, double c) {
        std::vector<double> w;
        for (int i = 0; i < train.size(); i++) {
            if (lambda[i] > 1e-8 && lambda[i] < c - 1e-8) {
                w.push_back(predict(train[i], train, lambda) - train[i].clazz);
            }
        }

        std::sort(w.begin(), w.end(), [](double a, double b) {
            return a - b;
        });
        return w.empty() ? 0 : w[w.size() / 2];
    }

    double lagrangian(std::vector<point> train, std::vector<double> lambda) {
        double res = 0;
        for (int i = 0; i < train.size(); i++) {
            res -= lambda[i];
            res += 0.5 * lambda[i] * lambda[i] * train[i].clazz * train[i].clazz * dist[i][i];
            for (int j = i + 1; j < train.size(); j++) {
                res += lambda[i] * lambda[j] * train[i].clazz * train[j].clazz * dist[i][j];
            }
        }
        return res;
    }


};

int main() {
    auto svmm = svm(std::cin);
    svmm.normalize();
    svmm.fit();
    svmm.output(std::cout);
    return 0;
}