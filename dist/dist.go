package main

import (
	"fmt"
	"sort"
)

type Pair struct {
	first, second int64
}
type byValues []Pair

func (s byValues) Len() int {
	return len(s)
}
func (s byValues) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byValues) Less(i,j int) bool {
	if (s[i].first < s[j].first) {
		return true
	} else if (s[i].first == s[j].first && s[i].second > s[j].second) {
		return true;
	} else {
		return false
	}
}

func main() {
	var k,n int
	fmt.Scan(&k, &n)
	x := make([]Pair, n)
	for i := 0; i < n; i++ {
		fmt.Scan(&x[i].first, &x[i].second);
		x[i].second -= 1;
	}
	sort.Sort(byValues(x))
	matrix := make([][]int64, k)
	ind := make([]int64, k)

	for i :=0; i < n; i++ {
		matrix[x[i].second] = append(matrix[x[i].second], int64(x[i].first))
	}
	var totalSum, ans int64
	for i := 0; i < n - 1; i++ {
		totalSum += (x[i+1].first - x[i].first) * (int64(n) - int64(i) - 1) * (int64(i) + 1)
		sz1 := int64(len(matrix[x[i].second]))
		ind1 := (ind[x[i].second])
		ind[x[i].second]++
		if (sz1 != ind1 + 1) {
			ans += (matrix[x[i].second][ind1 + 1] - matrix[x[i].second][ind1]) * (sz1 - ind1 - 1.0) * (ind1 + 1.0)
		}
	}

	fmt.Println(2 * ans)
	fmt.Println(2 * (totalSum - ans))
}
