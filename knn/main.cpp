#include <utility>

#include <iostream>
#include <vector>
#include <fstream>
#include <functional>
#include <algorithm>
#include <cmath>
#include <map>
#include <unordered_map>
#include <cstdlib>
#include <ctime>

const double PI = 3.14159265358979323846;
const double EPS = 1e-8;

typedef std::function<double (double,double)> Kernel;

namespace utils {
    double uniformKernel(double p, double h) {
        return 0.5 * (h > p);
    }

    double triangularKernel(double p, double h) {
        double rel = p / h;
        return (1 - rel) * (h > p);
    }

    double epanechnikovKernel(double p, double h) {
        double rel = p/h;
        return p >= h? 0 : 0.75 * ( 1 - pow(rel, 2));
    }

    double quarticKernel(double p, double h) {
        double rel = p / h;
        return p >= h ? 0 : 15.0 / 16.0 * pow(1 - rel * rel, 2);
    }

    double triweightKernel(double p, double h) {
        double rel = p / h;
        return p >= h ? 0 : 35.0 / 32.0 * pow(1 - rel * rel, 3);
    }

    double gaussianKernel(double p, double h) {
        double rel = p / h;
        return 1 / sqrt(2 * PI) * exp(-rel * rel / 2);
    }

    double calcF(const std::vector<std::vector<double>> &confus) {
        auto k = confus.size();
        std::vector<double> truePos(k), falsePos(k), falseNeg(k);
        double totalSum = 0;

        std::function<double (double, double)> f1 = [](double a, double b) {
            return 2 / ( 1/ a + 1/b);
        };

        for(int i = 0; i < k; i++) {
            truePos[i] = confus[i][i];
            for(int j = 0;j < k; j++) {
                falsePos[i] += confus[j][i] * (i != j);
                falseNeg[i] += confus[i][j] * (i != j);
            }
            totalSum += truePos[i] + falseNeg[i];
        }

        double micro = 0;
        for(int i = 0; i < k; i++) {
            double wei = double(truePos[i] + falseNeg[i]) / totalSum;
            double precision = !truePos[i] ? 0 : double(truePos[i]) / (truePos[i] + falsePos[i]);
            double recall = !truePos[i] ? 0 : double(truePos[i]) / (truePos[i] + falseNeg[i]);
            micro += f1(precision, recall) * wei;
        }

        return micro;
    }
}

const std::vector<Kernel> kernels = {
        utils::uniformKernel,
        utils::triangularKernel,
        utils::triweightKernel,
        utils::epanechnikovKernel,
        utils::gaussianKernel,
        utils::quarticKernel
};

class knnSolution {
    int n,m,l,q,kMax,pMax = 6, pBest =4, kBest = 1;
    std::vector<std::vector<double>> dist;


    class point {
        int i, clazz;
        std::vector<double> features;

    public:
        point() {
            i = 0;
            clazz = 0;
            features = std::vector<double>();
        }
        point(int i, std::vector<double> f, int c): i(i), features(std::move(f)), clazz(c) {}
        point(int i, std::istream &input, int n, bool train):i(i) {
            int tmp;
            for(int i = 0;i<n;i++) {
                input >> tmp;
                features.push_back(tmp);
            }
            if (train) {
                input >> clazz;
                clazz--;
            } else {
                clazz = 0;
            }
        }

        bool operator==(const point &other) const
        { return (i == other.i
                  && clazz == other.clazz);
        }

        friend knnSolution;
    };

    struct pointHasher {
        std::size_t operator()(const point& k) const
        {
            return ((std::hash<int>()(k.i)
                     ^ (std::hash<int>()(k.clazz) << 1)) >> 1);
        }
    };

    typedef std::unordered_map<point, double, pointHasher> pointMap;
    typedef std::pair<point, double> pointPair;

    Kernel kernel = kernels[0];
    std::vector<point> train;
    std::vector<point> test;

public:
    knnSolution(std::istream &input) {
        input >> n >> m >> l;
        dist = std::vector<std::vector<double>>(l + 1, std::vector<double>(l+1));
        kMax = 20;
        for(int i = 0; i < l; i++)
            train.push_back(point(i, input, n, true));

        input >> q;

        for(int i = 0; i < q; i++)
            test.push_back(point(l, input, n, false));
    }

    void normalize() {
        std::vector<double> mean(n);
        std::vector<double> dev(n);

        for(int i  = 0; i<n;i++) {
            for(auto &point: train) mean[i] += point.features[i];
            for(auto &point: test) mean[i] += point.features[i];

            mean[i] /= train.size() + test.size();
        }

        for(int i = 0; i < n;i++) {
            for(auto &point: train) {
                double x = point.features[i] - mean[i];
                dev[i] += pow(x,2);
            }
            for(auto &point: test) {
                double x = point.features[i] - mean[i];
                dev[i] += pow(x,2);
            }

            dev[i] /= train.size() + test.size() - 1;
            dev[i] = sqrt(dev[i]);
        }

        for(int i = 0; i < n; i++ ) {
            for(auto &point: train)
                point.features[i] = (dev[i] > EPS) * (point.features[i] - mean[i]) / dev[i];
            for(auto &point: test)
                point.features[i] = (dev[i] > EPS) * (point.features[i] - mean[i]) / dev[i];
        }
    }

    void loo() {
        double fBest = 0;
        std::vector<std::vector<point>> trainFolds(l);
        std::vector<std::vector<double>> confus(m, std::vector<double>(m, 0));

        for(int i = 0; i < l; i++) {
            for(int j = 0; j < l; j++) {
                trainFolds[i].push_back(train[j]);
            }
        }

        for (int p = 1; p <= pMax + 1; p++) {
            for (int i = 0; i < l; i++) {
                for (int j = i + 1; j < l; j++) {
                    dist[i][j] = dist[j][i] = calcP(train[i], train[j], p);
                }
            }
            for (int i = 0; i < l; i++) {
                std::sort(trainFolds[i].begin(), trainFolds[i].end(), [i, this](const point &a, const point &b) {
                    return dist[i][a.i] < dist[i][b.i];
                });
            }
            for (int i = 0; i < kernels.size(); i++) {
                for (int k = 1; k <= kMax; k++) {
                    confus = std::vector<std::vector<double>>(m, std::vector<double>(m, 0));
                    for (auto &pp : train) {
                        pointMap weights = calcWeights(pp, trainFolds[pp.i], k, kernels[i], true);
//                        for (auto const& weight: weights) {
//                            std::cout << weight.first.clazz << " " << weight.second << std::endl;
//                        }
//                        std::cout << "----------------------" << std::endl;
                        int c = predict(weights);
                        confus[pp.clazz][c]++;
                    }
                    double f = utils::calcF(confus);
                    if (f > fBest) {
                        fBest = f;
                        pBest = p;
                        kBest = k;
                        kernel = kernels[i];
                    }
                }
            }
        }
    }

    pointMap calcWeights(const point &a, std::vector<point> &train, int k, const Kernel &ker, bool offset) {
        std::unordered_map<point, double, pointHasher> map{};
        double h = dist[a.i][train[k + offset].i];
        for(int i = 0; i < k; i++) {
            auto q = train[i + offset];
            map[q] = h > EPS ? ker(dist[a.i][q.i], h) : 1.0;
        }

        return map;
    }

    int predict(const pointMap &weights) {
        std::unordered_map<int, double> map{};
        for (auto const& weight: weights) {
            map[weight.first.clazz] = weight.second;
        }

        double max = 0;
        std::vector<int> l{};

        for(auto const& entry: map) {
            if (entry.second > max) {
                max = entry.second;
                l.clear();
                l.push_back(entry.first);
            } else if (entry.second == max) {
                l.push_back(entry.first);
            }
        }

        return l[rand() % l.size()];
    }

    double calcP(const point &a1, const point &a2, int p) {
        if (p == 0) {
            return pmin(a1,a2);
        }
        else if (p == pMax + 1) {
            return pmax(a1,a2);
        }

        double res = 0;
        for (int i = 0; i < n; i++) {
            double x = fabs(a1.features[i] - a2.features[i]);
            double a = pow(x, double(p)/2.0);
            res += a;
        }
        return res;
    }

    double pmin(const point &a1, const point &a2) {
        double res = 0;
        for (int i = 0; i < n; i++) {
            double x = fabs(a1.features[i] - a2.features[i]);
            res = std::min(res, x);
        }
        return res;
    }

    double pmax(const point &a1, const point &a2) {
        double res = 0;
        for (int i = 0; i < n; i++) {
            double x = fabs(a1.features[i] - a2.features[i]);
            res = std::max(res, x);
        }
        return res;
    }

    void solution(std::ostream &out, std::istream &inTest) {
        std::vector<point> tmp{train.begin(), train.end()};
        int inputNumber;
        std::vector<std::vector<double>> cm(m, std::vector<double>(m));

        for(auto &testPoint:test) {
            for(int i = 0;i < l;i++) {
                dist[i][l] = dist[l][i] = calcP(train[i], testPoint, pBest);
            }

            std::sort(tmp.begin(), tmp.end(), [&testPoint, this](const point &a, const point &b) {
                return dist[l][a.i] < dist[l][b.i];
            });

            auto weights = calcWeights(testPoint, tmp, kBest, kernel, false);
            std::vector<pointPair> points{weights.begin(), weights.end()};
//
//            std::sort(points.begin(), points.end(), [](const pointPair &a, const pointPair &b) {
//                return a.second > b.second;
//            });

            std::cout << points.size() << " ";
            for(auto &x:points) {
                std::cout << x.first.i + 1 << " " << x.second << " ";
            }
            std::cout << std::endl;


            inTest >> inputNumber;
            inputNumber--;
            cm[inputNumber][predict(weights)]++;
        }

        double doubleTmp;
        inTest >> doubleTmp;
        std::cout << utils::calcF(cm) << std::endl;
        std::cout << doubleTmp << std::endl;
    }

    void solution(std::ostream &out) {
        auto tmp = train;

        for(auto &testPoint:test) {
            for(int i = 0;i < l;i++) {
                dist[i][l] = dist[l][i] = calcP(train[i], testPoint, pBest);
            }

            std::sort(tmp.begin(), tmp.end(), [&testPoint, this](const point &a, const point &b) {
                return dist[testPoint.i][a.i] < dist[testPoint.i][b.i];
            });

            auto weights = calcWeights(testPoint, tmp, kBest, kernel, false);
            std::vector<pointPair> points{weights.begin(), weights.end()};

            std::sort(points.begin(), points.end(), [](const pointPair &a, const pointPair &b) {
                return a.second > b.second;
            });

            std::cout << points.size() << " ";
            for(auto &x:points) {
                std::cout << x.first.i + 1 << " " << x.second << " ";
            }
            std::cout << std::endl;
        }
    }
};


int main(int argc, char *argv[]) {
    srand((int)time(0));
    std::fstream test = std::fstream("tests/01", std::fstream::in);
    std::fstream testAnswer = std::fstream("tests/01.a", std::fstream::in);
    auto knn = knnSolution(test);
    knn.normalize();
    knn.loo();
    knn.solution(std::cout, testAnswer);
    return 0;
}
